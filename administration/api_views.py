from django.conf import settings
from django.utils.decorators import method_decorator
from ratelimit.decorators import ratelimit
from rest_framework import viewsets

from administration.models import SchoolClass, Student
from administration.serializers import SchoolClassSerializer, StudentSerializer


def rate_limit(view):
    return ratelimit(key="ip", rate=settings.RATE_LIMIT, block=True)(view)


class SchoolClassBaseViewSet(viewsets.ModelViewSet):
    """Base class to use ratelimit in all default actions."""

    @method_decorator(rate_limit)
    def list(self, *args, **kwargs):
        return super().list(*args, **kwargs)

    @method_decorator(rate_limit)
    def create(self, *args, **kwargs):
        return super().create(*args, **kwargs)

    @method_decorator(rate_limit)
    def retrieve(self, *args, **kwargs):
        return super().retrieve(*args, **kwargs)

    @method_decorator(rate_limit)
    def update(self, *args, **kwargs):
        return super().update(*args, **kwargs)

    @method_decorator(rate_limit)
    def partial_update(self, *args, **kwargs):
        return super().partial_update(*args, **kwargs)

    @method_decorator(rate_limit)
    def destroy(self, *args, **kwargs):
        return super().destroy(*args, **kwargs)


class SchoolClassViewSet(SchoolClassBaseViewSet):
    queryset = SchoolClass.objects.all()
    serializer_class = SchoolClassSerializer


class StudentViewSet(SchoolClassBaseViewSet):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer
