from django.urls import include, path
from rest_framework import routers
from rest_framework.authtoken.views import obtain_auth_token


from administration.api_views import SchoolClassViewSet, StudentViewSet

router = routers.DefaultRouter()
router.register("schoolclasses", SchoolClassViewSet)
router.register("students", StudentViewSet)


urlpatterns = [
    path("", include(router.urls)),
    path("api-token-auth", obtain_auth_token)
]
