from django.test import TestCase
from administration.models import Student, SchoolClass
from django.db.utils import IntegrityError


class SchoolClassModelCreationTest(TestCase):
    def test_schoolclass_creation(self):
        school_class: SchoolClass = SchoolClass.objects.create(code="2022-001")
        self.assertIsNotNone(school_class.code)
        self.assertIsNotNone(school_class.pk)

    def test_code_duplication(self):
        school_class: SchoolClass = SchoolClass.objects.create(code="2022-001")
        with self.assertRaises(IntegrityError):
            clone: SchoolClass = SchoolClass.objects.create(
                code=school_class.code
            )


class StudentModelCreationTest(TestCase):
    def setUp(self):
        self.school_class: SchoolClass = SchoolClass.objects.create()

    def test_student_creation(self):
        student: Student = Student.objects.create(
            name="Willy Wonka",
            schoolclass=self.school_class,
            birth="1995-03-01",
            phone="+5584999999999",
            mother_name="Sra Wonka"
        )
        self.assertIsNotNone(student.pk)


class SchoolClassAPITest(TestCase):
    def test_list(self):
        self.school_class: SchoolClass = SchoolClass.objects.create(
            code="2022-ENG"
        )
        response = self.client.get("/api/v1/schoolclasses/")
        self.assertEqual(response.status_code, 200)
        json_response = response.json()
        self.assertEqual(len(json_response), 1)

    def test_create(self):
        response = self.client.post("/api/v1/schoolclasses/", data={
            "code": "2022POR"
        })
        self.assertEqual(response.status_code, 201)
        json_response = response.json()
        self.assertIsNotNone(json_response.get("id"))
