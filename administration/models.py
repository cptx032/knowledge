from datetime import date

from django.db import models


class SchoolClass(models.Model):
    code = models.CharField(max_length=7, unique=True)

    def __str__(self) -> str:
        return self.code


class Student(models.Model):
    name = models.CharField(max_length=1024)
    schoolclass = models.ForeignKey(
        SchoolClass, null=True, on_delete=models.CASCADE
    )
    birth = models.DateField(null=False)
    phone = models.CharField(max_length=255, unique=True)
    is_active = models.BooleanField(default=True)
    mother_name = models.CharField(max_length=1024)
