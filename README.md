# Knowledge - School API

A simple API to expose the registering of students and school classes.

## Installation Instructions
### Requirements
- Python 3.9

### Instalation Steps - Development Environment
- Run `python -m pip install virtualenv`
- Clone the project and enter in project folder in terminal
- Run `python -m virtualenv .venv`
- Run `pip install -r requirements-dev.txt`
- Create a copy of `.env.sample` with the name `.env`
- Run `./manage.py migrate`
- Run `./manage.py runserver`
- That is it! Your server is up running (in development mode)

## REST API
### API Docs
- Navigate to `http://knowledge-server-url.com/api/v1/`

### Creating access token
- make a `POST` to `/api/v1/api-token-auth` passing your username and password, for example:
```python
from typing import Dict
import requests

auth_data: Dict[str, str] = {
	"username": "john",
	"password": "lennon"
}
token: str = requests.post(
	"http://localhost:8000/api/v1/api-token-auth", data=auth_data
).json().get("token")
```

### Making requests to the API
- create your token and do:
```python
import requests

token: str = get_my_token("john", "lennon")
requests.get("my_url", headers={
	"Authorization": f"Token {token}"
})
```

## Contributing
- Run the unit tests: `./manage.py test --with-coverage`
- Before create any merge/pull request, run `./codeformat.sh` in your local code, it will format and check for eventual misuses.
