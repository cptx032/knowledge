Django==4.0.6
djangorestframework==3.13.1
django-environ==0.9.0
django-cors-headers==3.13.0
django-ratelimit==3.0.1
