#!/usr/bin/env bash

FILES=$(ls knowledge/.py administration/*.py)

echo "STARTING ISORT"
isort $FILES

echo "STARTING BLACK"
black -l 80 -t py39 $FILES

echo "STARTING PYFLAKES"
pyflakes $FILES

echo "STARTING MYPY"
mypy --no-warn-no-return --ignore-missing-imports --non-interactive --install-types $FILES